BOOST_AUTO_TEST_SUITE(forward_iterator_)

    BOOST_AUTO_TEST_CASE(post_increment_semantics_1_)
    {
        auto const s = "abc";
        my_iterator a = s, b = s;

        BOOST_REQUIRE(bool(a == b));
        BOOST_TEST(bool(a++ == b));
    }

    BOOST_AUTO_TEST_CASE(post_increment_semantics_2_)
    {
        auto const s = "abc";
        my_iterator a = s, b = s;

        BOOST_REQUIRE(bool(a == b));
        BOOST_TEST(bool(((void)a++, a) == ++b));
    }

    BOOST_AUTO_TEST_CASE(weakly_equality_comparable_)
    {
        auto const s = "abc";
        my_iterator t = s, u = s;

        BOOST_TEST((s == u));
        BOOST_TEST((u == s));

        BOOST_TEST(bool(t == u) == bool(u == t));
        BOOST_TEST(bool(t != u) == !bool(t == u));

        ++t;
        BOOST_CHECK(t != u);
        BOOST_CHECK(u != t);

        BOOST_TEST(bool(t == u) == bool(u == t));
        BOOST_TEST(bool(t != u) == !bool(t == u));
    }

    BOOST_AUTO_TEST_CASE(multi_pass_guarantee_)
    {
        auto const s = "abc";
        my_iterator i = s, j = s;

        BOOST_REQUIRE(i == j);
        BOOST_CHECK(++i == ++j);

        my_iterator k = i;
        ++k;
        BOOST_CHECK(i != k);
        BOOST_CHECK(*i == *j);

        auto const c = *i;
        BOOST_CHECK(((void)[](auto x) { ++x; }(i), *i) == c);
    }

    template<typename T>
    void
    pointer_validity_test(T)
    {
        if constexpr (std::is_reference_v<typename T::reference>) {
        }
    }

    BOOST_AUTO_TEST_CASE(pointer_is_valid_while_underline_range_is_alive_)
    {
#ifndef CANNOT_TAKE_VALUE_POINTER
        auto const s = "abc";
        char const* c = nullptr;

        {
            my_iterator i = s;
            c = &(*i);
        }

        BOOST_TEST(*c == 'a');
#endif
    }

BOOST_AUTO_TEST_SUITE_END() // forward_iterator_
