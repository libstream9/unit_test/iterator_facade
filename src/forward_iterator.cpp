#include <stream9/iterator_facade.hpp>

#include "function_test.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(forward_iterator_)

    BOOST_AUTO_TEST_SUITE(with_normal_value_)

        class my_iterator : public stream9::iterator_facade<
                                                my_iterator,
                                                std::forward_iterator_tag,
                                                char const >
        {
        public:
            my_iterator() = default;
            my_iterator(char const* const ptr) : m_ptr { ptr } {}

        private:
            friend class stream9::iterator_core_access;

            auto& dereference() const
            {
                return *m_ptr;
            }

            void increment()
            {
                ++m_ptr;
            }

            bool equal(my_iterator const& other) const
            {
                return m_ptr == other.m_ptr;
            }

        private:
            char const* m_ptr = nullptr;
        };

        static_assert(stream9::forward_iterator_base<my_iterator>);

        BOOST_AUTO_TEST_CASE(concept_)
        {
            static_assert(std::forward_iterator<my_iterator>);

            static_assert(std::same_as<std::iter_difference_t<my_iterator>, ptrdiff_t>);
            static_assert(std::same_as<std::iter_value_t<my_iterator>, char>);
            static_assert(std::same_as<std::iter_reference_t<my_iterator>, char const&>);
            static_assert(std::same_as<std::iter_rvalue_reference_t<my_iterator>, char const&&>);
        }

        BOOST_AUTO_TEST_CASE(function_availability_)
        {
            static_assert(has_dereference<my_iterator>);
            static_assert(!has_subscript<my_iterator>);
            static_assert(!has_pointer<my_iterator>);
            static_assert(has_increment<my_iterator>);
            static_assert(!has_decrement<my_iterator>);
            static_assert(!has_plus<my_iterator>);
            static_assert(!has_plus_equal<my_iterator>);
            static_assert(!has_minus<my_iterator>);
            static_assert(!has_minus_equal<my_iterator>);
            static_assert(has_equal<my_iterator>);
            static_assert(!has_equal<my_iterator, int>); // against some random type
            static_assert(!has_compare<my_iterator>);
            static_assert(!has_compare<my_iterator, int>); // against some random type
        }

        #include "input_iterator_semantics.cpp"
        #include "forward_iterator_semantics.cpp"

    BOOST_AUTO_TEST_SUITE_END() // with_normal_value_

    BOOST_AUTO_TEST_SUITE(with_proxy_value_)

        struct proxy
        {
            char c;

            operator char () const { return c; }
        };

        class my_iterator : public stream9::iterator_facade<
                                                my_iterator,
                                                std::forward_iterator_tag,
                                                proxy,
                                                proxy >
        {
        public:
            my_iterator() = default;
            my_iterator(char const* const ptr) : m_ptr { ptr } {}

        private:
            friend class stream9::iterator_core_access;

            proxy dereference() const
            {
                return { *m_ptr };
            }

            void increment()
            {
                ++m_ptr;
            }

            bool equal(my_iterator const& other) const
            {
                return m_ptr == other.m_ptr;
            }

        private:
            char const* m_ptr = nullptr;
        };

        static_assert(stream9::forward_iterator_base<my_iterator>);

        BOOST_AUTO_TEST_CASE(concept_)
        {
            static_assert(std::forward_iterator<my_iterator>);

            static_assert(std::same_as<std::iter_difference_t<my_iterator>, ptrdiff_t>);
            static_assert(std::same_as<std::iter_value_t<my_iterator>, proxy>);
            static_assert(std::same_as<std::iter_reference_t<my_iterator>, proxy>);
            static_assert(std::same_as<std::iter_rvalue_reference_t<my_iterator>, proxy>);
        }

        BOOST_AUTO_TEST_CASE(function_availability_)
        {
            static_assert(has_dereference<my_iterator>);
            static_assert(!has_subscript<my_iterator>);
            static_assert(!has_pointer<my_iterator>);
            static_assert(has_increment<my_iterator>);
            static_assert(!has_decrement<my_iterator>);
            static_assert(!has_plus<my_iterator>);
            static_assert(!has_plus_equal<my_iterator>);
            static_assert(!has_minus<my_iterator>);
            static_assert(!has_minus_equal<my_iterator>);
            static_assert(has_equal<my_iterator>);
            static_assert(!has_equal<my_iterator, int>); // against some random type
            static_assert(!has_compare<my_iterator>);
            static_assert(!has_compare<my_iterator, int>); // against some random type
        }

        #include "input_iterator_semantics.cpp"
        #define CANNOT_TAKE_VALUE_POINTER
        #include "forward_iterator_semantics.cpp"
        #undef CANNOT_TAKE_VALUE_POINTER


    BOOST_AUTO_TEST_SUITE_END() // value_with_proxy_object_

BOOST_AUTO_TEST_SUITE_END() // forward_iterator_

} // namespace testing
