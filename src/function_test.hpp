#ifndef STREAM9_XDG_TEST_FUNCTION_TEST_HPP
#define STREAM9_XDG_TEST_FUNCTION_TEST_HPP

namespace testing {

template<typename I>
inline constexpr bool has_dereference =
    requires (I&& i) {
        *i;
    };

template<typename I>
inline constexpr bool has_subscript =
    requires (I&& i, I::difference_type n) {
        i[n];
    };

template<typename I>
inline constexpr bool has_pointer =
    requires (I&& i) {
        i.operator->();
    };

template<typename I>
inline constexpr bool has_increment =
    requires (I& i) {
        ++i;
        i++;
    };

template<typename I>
inline constexpr bool has_decrement =
    requires (I& i) {
        --i;
        i--;
    };

template<typename I>
inline constexpr bool has_plus =
    requires (I&& i, I::difference_type n) {
        i + n;
        n + i;
    };

template<typename I>
inline constexpr bool has_plus_equal =
    requires (I& i, I::difference_type n) {
        i += n;
    };

template<typename I>
inline constexpr bool has_minus =
    requires (I&& i, I::difference_type n) {
        i - i;
        i - n;
    };

template<typename I>
inline constexpr bool has_minus_equal =
    requires (I& i, I::difference_type n) {
        i -= n;
    };

template<typename I, typename J = I>
inline constexpr bool has_equal =
    requires (I&& i, J&& j) {
        i == j;
    };

template<typename I, typename J = I>
inline constexpr bool has_compare =
    requires (I&& i, J&& j) {
        i <=> j;
    };

} // namespace testing

#endif // STREAM9_XDG_TEST_FUNCTION_TEST_HPP
