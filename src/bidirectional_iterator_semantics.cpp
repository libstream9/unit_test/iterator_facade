BOOST_AUTO_TEST_SUITE(bidirectional_iterator_)

    BOOST_AUTO_TEST_CASE(decrement_)
    {
        my_iterator it = "abc";
        ++it; ++it;

        --it;
        BOOST_TEST(*it == 'b');

        it--;
        BOOST_TEST(*it == 'a');
    }

    BOOST_AUTO_TEST_CASE(pre_decrement_yield_operands_lvalue_)
    {
        my_iterator it = "abc";
        ++it;

        BOOST_TEST(std::addressof(--it) == std::addressof(it));
    }

    BOOST_AUTO_TEST_CASE(post_decrement_yields_previous_value_)
    {
        auto const s = "abc";
        my_iterator a = s, b = s;

        ++a; ++b;
        BOOST_REQUIRE(a == b);

        BOOST_CHECK(a-- == b);
    }

    BOOST_AUTO_TEST_CASE(post_decrement_and_pre_decrement_perform_same_modification_)
    {
        auto const s = "abc";
        my_iterator a = s, b = s;

        ++a; ++b;
        BOOST_REQUIRE(a == b);

        a--; --b;
        BOOST_CHECK(a == b);
    }

    BOOST_AUTO_TEST_CASE(increment_and_decrement_are_inverse_of_each_other_)
    {
        auto const s = "abc";
        my_iterator a = s, b = s;

        ++a; ++b;
        BOOST_REQUIRE(a == b);

        BOOST_CHECK(++(--a) == b);
    }

BOOST_AUTO_TEST_SUITE_END() // bidirectional_iterator_
