
// random_access_iterator
BOOST_AUTO_TEST_CASE(advance_1_)
{
    auto const s = "abc";
    my_iterator a = s, b = s;

    ++b; ++b;
    my_iterator::difference_type const n = 2;

    BOOST_CHECK((a += n) == b);
}

BOOST_AUTO_TEST_CASE(advance_2_)
{
    auto const s = "abc";
    my_iterator a = s;
    my_iterator::difference_type const n = 2;

    BOOST_CHECK(std::addressof(a += n) == std::addressof(a));
}

BOOST_AUTO_TEST_CASE(advance_3_)
{
    auto const s = "abc";
    my_iterator a = s;
    my_iterator::difference_type const n = 2;

    auto i = a + n;
    auto j = a += n;
    BOOST_CHECK(i == j);
}

BOOST_AUTO_TEST_CASE(advance_4_)
{
    auto const s = "abc";
    my_iterator a = s;
    my_iterator::difference_type const n = 2;

    auto i = a + n;
    auto j = n + a;
    BOOST_CHECK(i == j);
}

BOOST_AUTO_TEST_CASE(advance_5_)
{
    auto const s = "abc";
    my_iterator a = s;

    auto i = a + (1 + 2);
    auto j = (a + 1) + 2;
    BOOST_CHECK(i == j);
}

BOOST_AUTO_TEST_CASE(advance_6_)
{
    auto const s = "abc";
    my_iterator a = s;

    BOOST_CHECK((a + 0) == a);
}

BOOST_AUTO_TEST_CASE(advance_7_)
{
    auto const s = "abc";
    my_iterator a = s, b = s;

    ++b; ++b;
    my_iterator::difference_type const n = 2;

    auto i = --b;
    auto j = a + (n - 1);
    BOOST_CHECK(i == j);
}

BOOST_AUTO_TEST_CASE(advance_8_)
{
    auto const s = "abc";
    my_iterator a = s, b = s;

    ++b; ++b;
    my_iterator::difference_type const n = 2;

    auto b1 = b, b2 = b;
    auto i = b1 += -n;
    auto j = b2 -= n;
    BOOST_CHECK(i == a);
    BOOST_CHECK(j == a);
}

BOOST_AUTO_TEST_CASE(advance_9_)
{
    auto const s = "abc";
    my_iterator b = s;
    my_iterator::difference_type const n = 2;

    ++b; ++b;

    BOOST_CHECK(std::addressof(b -= n) == std::addressof(b));
}

BOOST_AUTO_TEST_CASE(advance_10_)
{
    auto const s = "abc";
    my_iterator b = s;
    my_iterator::difference_type const n = 2;

    ++b; ++b;

    auto i = b - n;
    auto j = (b -= n);
    BOOST_CHECK(i == j);
}

BOOST_AUTO_TEST_CASE(subscript_)
{
    auto const s = "abc";
    my_iterator a = s, b = s;

    ++b; ++b;
    my_iterator::difference_type const n = 2;

    BOOST_TEST(a[n] == *b);
}

BOOST_AUTO_TEST_CASE(comparison_)
{
    auto const s = "abc";
    my_iterator a = s, b = s;

    ++b; ++b;

    BOOST_TEST(bool(a <= b));
}

